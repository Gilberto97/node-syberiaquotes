# SyberiaQuotes

### Introduction

SyberiaQuotes is a REST-API web application whose main assumption is the archiving of the lyrics of songs and searching for their titles by a fragment of the lyrics provided by the user.

The application was created for fans of the music band LSO (Liryka Skladu Ostrodzkiego) from Ostroda (PL), which since 2002 has released over 1000 songs! Most of the songs are spread over various YouTube channels, and lyrics can be found for less than 1% of them. Fans, despite the remembered part of the lyrics, are often unable to find the title of the song.

Thanks to our application, the user, after entering a fragment of a song, can find the title of the song and listen to it using a link to the YouTube service kept along with the title and text in our database.

Our platform is autonomous - after creating an account, the user can add new lyrics which will be added to the official database after verification.

### Build With
* [JavaScript](https://www.javascript.com/)
* [Node.js](https://nodejs.org/en/)
* [Express.js](https://expressjs.com/)
* [MySQL](https://www.mysql.com/)
* [Docker](https://docs.docker.com/)

... and love to LSO :)
## Prerequisites

#### Docker Engine
To run the application locally, You need [Docker](https://docs.docker.com/get-docker/) and [Docker-Compose](https://docs.docker.com/compose/install/). Make sure you have them installed on your operating system

```bash
$ docker -v
Docker version 20.10.3, build 48d30b5
```

```bash
$ docker-compose -v
docker-compose version 1.25.5, build unknown
```
## Getting started

At the very beginning, we need to clone the repository to access it on the local machine:

```bash
git clone https://gitlab.com/Gilberto97/node-syberiaquotes.git
```

Then go to the downloaded folder:
```bash
cd node-syberiaquotes
```


#### Environment Variables

After cloning the repository on your device, in the main application directory, create a file *.env* in which the environment variables needed to run the application will be stored.

**.env** :
```txt
PORT=3000
SQL_DB=syberiaquotes
SQL_USER=root
SQL_PASSWORD=secret123
SQL_HOST=db
TOKEN_SECRET=c655076e-c1b9-49f8-8278-190e41348093
```
### Running locally with Docker

After creating the file with the environment variables, we can let Docker build needed images and then run the containers locally. We are doing it with the following command:

```bash
docker-compose up
```

The containers can be run in detached mode so that they run in the background:

```bash
docker-compose up -d
```

You can make sure the containers are running the following command:

```bash
$ docker ps
CONTAINER ID   IMAGE                    COMMAND                  CREATED              STATUS          PORTS                               NAMES
7e362e6c51bf   node-syberiaquotes_web   "docker-entrypoint.s…"   46 seconds ago       Up 10 seconds   0.0.0.0:3000->3000/tcp              syberiaquotes-restapi
8bf914e50468   mysql:latest             "docker-entrypoint.s…"   About a minute ago   Up 10 seconds   0.0.0.0:3306->3306/tcp, 33060/tcp   syberiaquotes-sql

```

### License
Distributed under the MIT License.

### Authors:
Przemyslaw Baj

