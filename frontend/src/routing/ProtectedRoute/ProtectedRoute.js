import './ProtectedRoute.scss';

import { Redirect, Route } from 'react-router-dom';
// context hook
import { useAppContext } from '../../AppContext';

// static
import { PATHNAMES } from '../../static/headerStatic';

const ProtectedRoute = ({ adminPermissions, component, ...rest }) => {
  const Component = component;
  const { isAdmin, isUserLogged } = useAppContext();

  const route = <Route {...rest} render={() => <Component {...rest} />} />;

  if (isUserLogged) {
    if (adminPermissions)
      return isAdmin ? (
        <Route {...rest} render={() => <Component {...rest} />} />
      ) : (
        <Redirect to={PATHNAMES.HOME} />
      );
    return <Route {...rest} render={() => <Component {...rest} />} />;
  } else {
    return <Redirect to={PATHNAMES.LOGIN} />;
  }
};

export default ProtectedRoute;
