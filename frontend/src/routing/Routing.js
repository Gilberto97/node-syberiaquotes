import './Routing.scss';

import { Container } from 'react-bootstrap';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

// layouts
import Header from '../layouts/Header/Header';
import Home from '../layouts/Home/Home';
import NotFound404 from '../layouts/NotFound404/NotFound404';

// components
import Profile from '../components/Profile/Profile';

// pages
import Contact from '../pages/Contact/Contact';
import LogoutPage from '../pages/LogoutPage/LogoutPage';
import NewSongPage from '../pages/NewSongPage/NewSongPage';
import SongListPage from '../pages/SongListPage/SongListPage';
import SongPage from '../pages/SongPage/SongPage';
import PrivacyPolicy from '../pages/PrivacyPolicy/PrivacyPolicy';
import SingInPage from '../pages/SingInPage/SingInPage';
import SingUpPage from '../pages/SingUpPage/SingUpPage';
import VerifyListPage from '../pages/VerifyListPage/VerifyListPage';
import VerifySongPage from '../pages/VerifySongPage/VerifySongPage';

// Protected Route
import ProtectedRoute from './ProtectedRoute/ProtectedRoute';

// Context
import AppProvider from '../AppContext';

const Routing = () => {
  return (
    <Router>
      <AppProvider>
        <Header />
        <Container>
          <Switch>
            <ProtectedRoute path="/songs/new" component={NewSongPage} />
            <ProtectedRoute exact path="/verify" adminPermissions={true} component={VerifyListPage} />
            <ProtectedRoute path="/verify/:id" adminPermissions={true} component={VerifySongPage} />
            <Route path="/songs/:id/:edit?" component={SongPage} />
            <Route path="/contact" component={Contact} />
            <ProtectedRoute path="/logout" component={LogoutPage} />
            <Route path="/privacy-policy" component={PrivacyPolicy} />
            <ProtectedRoute path="/profile" component={Profile} />
            <Route path="/singUp" component={SingUpPage} />
            <Route path="/singIn" component={SingInPage} />
            <Route exact path="/songs" component={SongListPage} />
            <Route exact path="/" component={Home} />
            <Route component={NotFound404} />
          </Switch>
        </Container>
      </AppProvider>
    </Router>
  );
};

export default Routing;
