import './App.scss';

import Router from './routing/Routing';

import Footer from './layouts/Footer/Footer';

const App = () => {
  return (
    <div className="App">
      <Router />
      <Footer />
    </div>
  );
};

export default App;
