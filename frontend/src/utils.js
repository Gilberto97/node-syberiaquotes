export const isSuccess = (code) => [200, 201].includes(code);

export const setLocalStorageItem = (key, value) => localStorage.setItem(key, JSON.stringify(value));
export const removeLocaleStorageItem = (key) => localStorage.removeItem(key);
