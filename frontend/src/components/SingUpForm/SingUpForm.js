import './SingUpForm.scss';

import { useState } from 'react';
import { Link } from 'react-router-dom';

// components
import Loading from '../Loading/Loading';
import ErrorMessage from '../ErrorMessage/ErrorMessage';
import SuccessMessage from '../SuccessMessage/SuccessMessage';

// hooks
import usePostJson from '../../hooks/usePostJson';

// static
import { MESSAGES } from '../../static/messageStatic';

// static url
const singUpUrl = 'auth/signup';

const SingUpForm = () => {
  const [name, setName] = useState('');
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const [agree, setAgree] = useState(false);
  const { isLoading, errors, isError, data, postData } = usePostJson(singUpUrl);

  const handleNameChange = (e) => setName(e.target.value);

  const handleEmailChange = (e) => setEmail(e.target.value);

  const handlePasswordChange = (e) => setPassword(e.target.value);

  const handleAgreeChange = () => setAgree((prev) => !prev);

  const checkValid = () => {
    if (name.length <= 0 || email.length <= 0 || password.length <= 0 || !agree)
      return false;
    return true;
  };

  const clearForm = () => {
    setName('');
    setEmail('');
    setPassword('');
    setAgree(false);
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (checkValid()) {
      const newUser = { email, name, password };
      await postData(newUser);
      clearForm();
    }
  };

  return (
    <>
      <form className="sing-up__form" onSubmit={handleSubmit}>
        <label htmlFor="name">
          <input
            id="name"
            type="text"
            placeholder="Name"
            value={name}
            onChange={handleNameChange}
            required
          />
        </label>
        <label htmlFor="email">
          <input
            id="email"
            type="email"
            placeholder="Email"
            value={email}
            onChange={handleEmailChange}
          />
        </label>
        <label htmlFor="password">
          <input
            id="password"
            type="password"
            placeholder="Password"
            value={password}
            onChange={handlePasswordChange}
            required
          />
        </label>
        <label htmlFor="agree">
          <input
            id="agree"
            type="checkbox"
            value={agree}
            onChange={handleAgreeChange}
            required
          />
          <span>
            Agree <Link to="/privacy-policy">privacy policy</Link>
          </span>
        </label>
        <button className="sing-up__form-btn" type="submit">
          Sign Up
        </button>
      </form>
      {isLoading && <Loading text={MESSAGES.LOADING} />}
      {isError && <ErrorMessage errors={errors} />}
      {!isError && !isLoading && data && (
        <SuccessMessage text={MESSAGES.REGISTER_SUCCESS} />
      )}
    </>
  );
};

export default SingUpForm;
