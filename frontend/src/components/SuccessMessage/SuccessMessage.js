import './SuccessMessage.scss';

const SuccessMessage = ({text}) => {
  return (
    <div className="success-message">
      <h5 className="success-message__text">{text}</h5>
    </div>
  );
}
 
export default SuccessMessage;