import './NewSongForm.scss';

import { useState } from 'react';

//context
import { useAppContext } from '../../AppContext';

// components
import Loading from '../Loading/Loading';
import ErrorMessage from '../ErrorMessage/ErrorMessage';
import SuccessMessage from '../SuccessMessage/SuccessMessage';

// hooks
import usePostJson from '../../hooks/usePostJson';

// static
import { MESSAGES } from '../../static/messageStatic';
import { PATHNAMES } from '../../static/headerStatic';

// static url
const postNewSongUrl = PATHNAMES.SONGS;

const NewSongForm = () => {
  const [title, setTitle] = useState('');
  const [lyrics, setLyrics] = useState('');
  const [link, setLink] = useState('');

  const { userToken } = useAppContext();

  const { isLoading, errors, isError, data, postData } = usePostJson(
    postNewSongUrl,
    userToken
  );
  //input handler
  const handleInputChange = (e) => {
    switch (e.target.id) {
      case 'title':
        setTitle(e.target.value);
        break;
      case 'lyrics':
        setLyrics(e.target.value);
        break;
      case 'link':
        setLink(e.target.value);
        break;
    }
  };

  //form validation (to be improved)
  const formValidation = () => {
    if (title.length <= 0 || lyrics.length <= 0 || link.length <= 0)
      return false;
    return true;
  };

  const clearForm = () => {
    setTitle('');
    setLyrics('');
    setLink('');
  };

  //form submit handler
  const handleFormSubmit = async (e) => {
    e.preventDefault();

    if (formValidation()) {
      const newSong = { title, lyrics, link };

      await postData(newSong);
      clearForm();
    }
  };

  return (
    <>
      {!isError && !isLoading && data ? (
        <SuccessMessage text={MESSAGES.ADDSONG_SUCCESS} />
      ) : (
        <>
          <form onSubmit={handleFormSubmit}>
            <input
              id="title"
              onChange={handleInputChange}
              value={title}
              type="text"
              placeholder="Tytuł"
            />
            <textarea
              id="lyrics"
              onChange={handleInputChange}
              value={lyrics}
              placeholder="Tekst"
            />
            <input
              id="link"
              onChange={handleInputChange}
              value={link}
              type="text"
              placeholder="Link do YouTube"
            />
            <button type="submit">Dodaj</button>
          </form>
        </>
      )}
      {isLoading && <Loading text={MESSAGES.LOADING} />}
      {isError && <ErrorMessage errors={errors} />}
    </>
  );
};

export default NewSongForm;
