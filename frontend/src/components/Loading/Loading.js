import './Loading.scss';

const Loading = ({text}) => {
  return (
    <div className="loading">
      <h5 className="loading-text">{text}</h5>
    </div>
  );
}
 
export default Loading;