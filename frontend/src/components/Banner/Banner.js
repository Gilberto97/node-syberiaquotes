import './Banner.scss';

const Banner = ({ title, gifType }) => {
  return (
    <div className="banner" id={gifType}>
      <h1 className="banner-title">{title}</h1>
    </div>
  );
};

export default Banner;
