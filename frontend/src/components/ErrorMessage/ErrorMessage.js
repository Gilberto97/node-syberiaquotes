import './ErrorMessage.scss';

const ErrorMessage = ({ errors }) => {
  const currentErrors = errors?.map(error => (
    <li key={error.param}>{error.msg}</li>
  ));

  return <ul className="errors">{currentErrors}</ul>;
};

export default ErrorMessage;
