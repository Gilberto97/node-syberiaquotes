import './SongList.scss';

// react
import { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';

// components
import Loading from '../Loading/Loading';

// hooks
import useGetJson from '../../hooks/useGetJson';

// static
import { MESSAGES } from '../../static/messageStatic';
import { PATHNAMES } from '../../static/headerStatic';

// static url
const songsUrl = PATHNAMES.SONGS;
const verifySongsUrl = PATHNAMES.VERIFY_SONGS;

const SongList = ({ verify }) => {
  const [songList, setSongList] = useState([]);
  const { isLoading, isError, data, getData } = useGetJson(
    verify ? verifySongsUrl : songsUrl
  );
  // const singleSongPath = verify ? PATHNAMES.VERIFY : PATHNAMES.SONGS

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => setSongList(await getData());

  const songs = songList.length > 0 && songList?.map(({ id, title }) => (
    <li key={id}>
      <h5>{title}</h5>
      <Link to={`verify/${id}`}>Wiecej</Link>
    </li>
  ));

  return (
    <>
      {!isError && !isLoading && data && <ul className="song-list">{songs}</ul>}
      {isLoading && <Loading text={MESSAGES.LOADING} />}
    </>
  );
};

export default SongList;