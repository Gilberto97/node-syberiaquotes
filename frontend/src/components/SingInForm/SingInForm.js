import './SingInForm.scss';

import { useState } from 'react';

// components
import Loading from '../Loading/Loading';
import ErrorMessage from '../ErrorMessage/ErrorMessage';

// hooks
import usePostJson from '../../hooks/usePostJson';

// context
import { useAppContext } from '../../AppContext';

// static
import { MESSAGES } from '../../static/messageStatic';

// static url
const singInUrl = 'auth/signin';

const SingIn = () => {
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
  const { isLoading, errors, isError, postData } = usePostJson(singInUrl);
  const { loginUser } = useAppContext();

  const handleEmailChange = (e) => setEmail(e.target.value);

  const handlePasswordChange = (e) => setPassword(e.target.value);

  const checkValid = () => {
    if (email.length <= 0 || password.length <= 0) return false;
    return true;
  };

  const handleSubmit = async (e) => {
    e.preventDefault();

    if (checkValid()) {
      const user = { email, password };
      try {
        loginUser(await postData(user));
      } catch (e) {
        console.log(e);
      }
    }
  };

  return (
    <>
      <form className="sing-in__form" onSubmit={handleSubmit}>
        <label htmlFor="email">
          <input
            id="email"
            type="email"
            placeholder="email (email)"
            value={email}
            onChange={handleEmailChange}
            required
          />
        </label>
        <label htmlFor="password">
          <input
            id="password"
            type="password"
            placeholder="password"
            value={password}
            onChange={handlePasswordChange}
            required
          />
        </label>
        <button className="sing-in__form-btn" type="submit">
          Log in
        </button>
      </form>
      {isLoading && <Loading text={MESSAGES.LOADING} />}
      {isError && <ErrorMessage errors={errors} />}
    </>
  );
};

export default SingIn;
