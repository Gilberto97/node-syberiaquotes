import './Song.scss';

// react
import React from 'react';
import { useState, useEffect } from 'react';

// react-router
import { useHistory, useParams } from 'react-router-dom';

//context
import { useAppContext } from '../../AppContext';

// components
import Loading from '../Loading/Loading';
import SuccessMessage from '../SuccessMessage/SuccessMessage';

// hooks
import useDeleteJson from '../../hooks/useDeleteJson';
import useGetJson from '../../hooks/useGetJson';
import usePatchJson from '../../hooks/usePatchJson';

// static
import { MESSAGES } from '../../static/messageStatic';
import { PATHNAMES } from '../../static/headerStatic';

// static url
const songUrl = PATHNAMES.SONGS;

const Song = ({ verify }) => {
  const { id, edit } = useParams();
  const history = useHistory();
  const { isAdmin, userToken } = useAppContext();

  const [title, setTitle] = useState('');
  const [lyrics, setLyrics] = useState('');
  const [link, setLink] = useState('');
  const [readonly, setReadonly] = useState(!edit);

  //getting song hook
  const { isLoading, getData } = useGetJson(`${songUrl}/${id}`);

  //update song hook
  const {
    isLoading: patching,
    isPatchError,
    data: patData,
    patchData
  } = usePatchJson(songUrl + id, userToken);

  //deleting song hook
  const {
    isError: isDeleteError,
    isLoading: deleting,
    data: delData,
    deleteData
  } = useDeleteJson(songUrl + id, userToken);

  useEffect(() => {
    loadData();
  }, []);

  const loadData = async () => setSongData(await getData());

  const setSongData = (data) => {
    setTitle(data?.title);
    setLyrics(data?.lyrics);
    setLink(data?.link);
  };

  const handleInputChange = (e) => {
    switch (e.target.id) {
      case 'title':
        setTitle(e.target.value);
        break;
      case 'lyrics':
        setLyrics(e.target.value);
        break;
      case 'link':
        setLink(e.target.value);
        break;
    }
  };

  const handleEditClick = (e) => {
    e.preventDefault();
    setReadonly((prev) => !prev);
  }

  const handleDeleteClick = () => deleteData();

  const isValid = () => {
    if (title.length <= 0 || lyrics.length <= 0 || link.length <= 0) return false;
    return true;
  };

  const handleFormSubmit = async (e) => {
    e.preventDefault();

    if (isValid()) {
      const song = {title, lyrics, link, isActive: true};
  
      await patchData(song);
      // setTimeout(() => {
      //   history.push(verify ? '/verify' : '/songs');
      // }, 3000);
    }
  };

  return (
    <div className="song">
      <div className="song-buttons">
        {isAdmin && readonly && <button onClick={handleFormSubmit}>{ verify ? 'Apply' : 'Update' }</button>}
        {isAdmin && readonly && <button onClick={handleEditClick}>Edit</button>}
        {isAdmin && !readonly && <button onClick={handleDeleteClick}>Delete</button>}
      </div>
      {!isDeleteError && !deleting && delData
        ?
        <SuccessMessage text={MESSAGES.DELETESONG_SUCCESS} />
        :
      <form className="song-form" onSubmit={handleEditClick}>
        <input id="title"
          disabled={readonly}
          onChange={handleInputChange}
          placeholder="Tytuł"
          required
          value={title}
          type="text"
        />
        <input id="link"
          disabled={readonly}
          onChange={handleInputChange}
          placeholder="Link do YouTube"
          required
          value={link}
          type="text"
        />
        <textarea id="lyrics"
          disabled={readonly}
          onChange={handleInputChange}
          placeholder="Tekst"
          required
          value={lyrics}
        />
        {!readonly && <button type="submit">Submit</button>}
      </form>}
      {/* Loading */}
      {isLoading && <Loading text={MESSAGES.LOADING} />}
      {patching && <Loading text={MESSAGES.PATCHING} />}
      {deleting && <Loading text={MESSAGES.DELETING} />}
      {/* Messages */}
      {!isPatchError && !patching && patData && <SuccessMessage text={MESSAGES.PATCHSONG_SUCCESS} />}
    </div>
  );
};

export default Song;
