import { useState } from 'react';

import { isSuccess } from '../utils';

import { ROOT_URL } from '../static/urls';

const usePatchJson = (url, authToken) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [errors, setErrors] = useState(null);
  const [data, setData] = useState(null);

  const path = ROOT_URL + url;

  const patchData = async (data) => {
    try {
      setIsLoading(true);
      setIsError(false);
      const response = await fetch(path, {
        method: 'PATCH',
        headers: {
          'Authorization': 'Bearer ' + authToken,
          'Content-Type': 'application/json',
        },
        body: JSON.stringify(data),
      });
      if (!isSuccess(response.status)) {
        const { errors } = await response.json();
        setIsError(true);
        setErrors(errors);
        return;
      }

      const json = await response.json();
      setData(json);
      return json;
    } catch (e) {
      setIsError(true);
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };
  return {
    isLoading,
    errors,
    isError,
    data,
    patchData,
  };
};

export default usePatchJson;
