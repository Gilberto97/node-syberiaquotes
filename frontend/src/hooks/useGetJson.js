import { useState } from "react";

import { isSuccess } from "../utils";

import { ROOT_URL } from "../static/urls";

const useGetJson = (url) => {
  const [isLoading, setIsLoading] = useState(false);
  const [isError, setIsError] = useState(false);
  const [errors, setErrors] = useState(null);
  const [data, setData] = useState(null);

  const path = ROOT_URL + url;

  const getData = async () => {
    try {
      setIsLoading(true);
      setIsError(false);
      const response = await fetch(path, {
        method: "GET",
        headers: {
          "Content-Type": "application/json",
        },
      });
      if (!isSuccess(response.status)) {
        const { errors } = await response.json();
        setIsError(true);
        setErrors(errors);
        return;
      }
      const json = await response.json();
      setData(json);
      return json;
    } catch (e) {
      setIsError(true);
      // setError(e);
      console.log(e);
    } finally {
      setIsLoading(false);
    }
  };
  return {
    isLoading,
    errors,
    isError,
    data,
    getData,
  };
};

export default useGetJson;
