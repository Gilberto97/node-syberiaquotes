const NotFound404 = () => {
  return (
    <h1>Ups! Page not found</h1>
  );
}
 
export default NotFound404;