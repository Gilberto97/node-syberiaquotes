import './Nav.scss';
import { NavLink } from 'react-router-dom';

import { useAppContext } from '../../AppContext';

import { AUTH_PATHS, PATHS } from '../../static/headerStatic';

const Nav = () => {
  const { isUserLogged } = useAppContext();
  const paths = isUserLogged ? AUTH_PATHS : PATHS;

  const links = paths?.map(({ id, pathname, title }) => {
    return (
      <li key={id}>
        <NavLink to={pathname}>{title}</NavLink>
      </li>
    );
  });

  return (
    <nav>
      <ul className="nav-list">{links}</ul>
    </nav>
  );
};

export default Nav;
