import './SearchBar.scss';

const SearchBar = ({ placeholder }) => {
  return (
    <form className="searchBar">
      <input type="text" placeholder={placeholder} />
    </form>
  );
};

export default SearchBar;
