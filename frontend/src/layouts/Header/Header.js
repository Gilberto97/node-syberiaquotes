import './Header.scss';
import logo from '../../assets/images/logo.png';
import { Link } from 'react-router-dom';

import Nav from '../Nav/Nav';
import SearchBar from '../SearchBar/SearchBar';

import { searchBarPlaceholder } from '../../static/headerStatic';

const Header = () => {
  return (
    <div className="header">
      <div className="header-logo">
        <Link to="/">
          <img src={logo} className="header-logo__img" alt="logo" />
        </Link>
        <h1 className="header-logo__title">SyberiaQuotes</h1>
      </div>
      <Nav/>
      <SearchBar placeholder={searchBarPlaceholder}/>
    </div>
  );
};

export default Header;
