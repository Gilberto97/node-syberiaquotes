export const BANNER_TYPES = {
  CONTACT: 'CONTACT',
  NEW_SONG: 'NEW_SONG',
  PRIVACY_POLICY: 'PRIVACY_POLICY',
  PROFILE: 'PROFILE',
  SING_IN: 'SING_IN',
  SING_UP: 'SING_UP',
  SONG: 'SONG',
  SONG_LIST: 'SONG_LIST',
  VERIFY_LIST: 'VERIFY_LIST',
  VERIFY_SONG: 'VERIFY_SONG',
};

export const BANNER_TITLES = {
  CONTACT: 'CONTACT',
  NEW_SONG: 'Dodaj piosenke',
  PRIVACY_POLICY: 'PRIVACY POLICY',
  PROFILE: 'PROFILE',
  SING_IN: 'Zaloguj',
  SING_UP: 'Zarejestruj sie',
  SONG: 'Piosenka',
  SONG_LIST: 'Lista Piosenek',
  VERIFY_LIST: 'Lista do weryfikacji',
  VERIFY_SONG: 'Weryfikacja piosenki',
};
