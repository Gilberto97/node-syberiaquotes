export const MESSAGES = {
  REGISTER_SUCCESS: 'Registration successfully!',
  ADDSONG_SUCCESS: 'Piosenka dodana. Dzieki!',
  PATCHSONG_SUCCESS: 'Piosenka została zmieniona',
  DELETESONG_SUCCESS: 'Piosenka została usunięta',
  LOADING: 'Loading...',
  DELETING: 'Deleting...',
  PATCHING: 'Updating...'
};
