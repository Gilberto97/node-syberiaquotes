export const PATHNAMES = {
  CONTACT: '/contact',
  HOME: '/',
  LOGIN: '/singIn',
  LOGOUT: '/logout',
  NEW_SONG: '/songs/new',
  PROFILE: '/profile',
  REGISTER: '/singUp',
  SONGS: '/songs',
  VERIFY: '/verify',
  VERIFY_SONGS: '/songs/verify',
};

export const PATHS = [
  { id: 1, pathname: PATHNAMES.LOGIN, title: 'Zaloguj' },
  { id: 2, pathname: PATHNAMES.REGISTER, title: 'Zarejestruj' },
  { id: 3, pathname: PATHNAMES.SONGS, title: 'Lista piosenek' },
  { id: 4, pathname: PATHNAMES.CONTACT, title: 'Kontakt' },
];

export const AUTH_PATHS = [
  { id: 1, pathname: PATHNAMES.SONGS, title: 'Lista piosenek' },
  { id: 2, pathname: PATHNAMES.NEW_SONG, title: 'Dodaj piosenke' },
  { id: 3, pathname: PATHNAMES.CONTACT, title: 'Kontakt' },
  { id: 4, pathname: PATHNAMES.PROFILE, title: 'Profil' },
  { id: 5, pathname: PATHNAMES.VERIFY, title: 'Lista weryfikacyjna' },
  { id: 6, pathname: PATHNAMES.LOGOUT, title: 'Wyloguj' },
];

export const searchBarPlaceholder = 'Znajdz piosenke ktora chodzi Ci po glowie';
