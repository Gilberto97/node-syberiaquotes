import { createContext, useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';

import { removeLocaleStorageItem, setLocalStorageItem } from './utils';

import { STORAGES } from './static/storageKeys';

export const AppContext = createContext();

const AppProvider = ({ children }) => {
  const [isUserLogged, setIsUserLogged] = useState(false);
  const [userToken, setUserToken] = useState('');
  const [isAdmin, setIsAdmin] = useState(true);
  const history = useHistory();

  const getUserLocalStorageItem = (key) => {
    const userLocalStorage = JSON.parse(
      localStorage.getItem(STORAGES.USER_INFO)
    );
    return userLocalStorage ? userLocalStorage[key] : null;
  };

  useEffect(() => {
    setIsAdmin(getUserLocalStorageItem('isAdmin'));
    setIsUserLogged(getUserLocalStorageItem('isUserLogged'));
    setUserToken(getUserLocalStorageItem('userToken'));
  }, [isAdmin, isUserLogged, userToken]);

  const loginUser = ({ token }) => {
    if (token) {
      setIsAdmin(true);
      setIsUserLogged(true);
      setUserToken(token);
      setLocalStorageItem(STORAGES.USER_INFO, {
        isAdmin: true,
        isUserLogged: true,
        userToken: token,
      });
      history.replace('/profile');
    }
  };

  const logoutUser = () => {
    setIsUserLogged(false);
    setUserToken('');
    setIsAdmin(false);
    removeLocaleStorageItem(STORAGES.USER_INFO);
  };

  return (
    <AppContext.Provider
      value={{ loginUser, logoutUser, isAdmin, isUserLogged, userToken }}
    >
      {children}
    </AppContext.Provider>
  );
};

export const useAppContext = () => useContext(AppContext);
export default AppProvider;
