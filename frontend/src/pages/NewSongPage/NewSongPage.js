import './NewSongPage.scss';

import Banner from '../../components/Banner/Banner';
import NewSongForm from '../../components/NewSongForm/NewSongForm';

import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const NewPageSong = () => {
  return (
    <div className="new-song">
      <Banner title={BANNER_TITLES.NEW_SONG} gifType={BANNER_TYPES.NEW_SONG} />
      <NewSongForm />
    </div>
  );
};

export default NewPageSong;
