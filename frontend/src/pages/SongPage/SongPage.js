import './SongPage.scss';

import Banner from '../../components/Banner/Banner';
import Song from '../../components/Song/Song';

import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const SongPage = () => {
  return (
    <div>
      <Banner title={BANNER_TITLES.SONG} gifType={BANNER_TYPES.SONG} />
      <Song />
    </div>
  );
};

export default SongPage;
