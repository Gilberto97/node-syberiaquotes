import './SongListPage.scss';

// components
import Banner from '../../components/Banner/Banner';
import SongList from '../../components/SongList/SongList';

// static
import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const SongListPage = () => {
  return (
    <div>
      <Banner
        title={BANNER_TITLES.SONG_LIST}
        gifType={BANNER_TYPES.SONG_LIST}
      />
      <SongList />
    </div>
  );
};

export default SongListPage;
