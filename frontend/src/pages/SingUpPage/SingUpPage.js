import './SingUpPage.scss';

import Banner from '../../components/Banner/Banner';
import SingUpForm from '../../components/SingUpForm/SingUpForm';

import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const SingUpPage = () => {
  return (
    <div className="sing-up">
      <Banner title={BANNER_TITLES.SING_UP} gifType={BANNER_TYPES.SING_UP} />
      <SingUpForm />
    </div>
  );
};

export default SingUpPage;
