import './LogoutPage.scss';

import { useHistory } from 'react-router-dom';

// Context
import { useAppContext } from '../../AppContext';
import { useEffect } from 'react';

const LogOutPage = () => {
  const { logoutUser } = useAppContext();
  const history = useHistory();

  useEffect(() => {
    logoutUser();
    const timeoutIndex = setTimeout(() => {
      history.replace('/');
    }, 3000);

    return () => clearTimeout(timeoutIndex);
  }, []);

  return <p>Pomyślnie wylogowano!</p>;
};

export default LogOutPage;
