import './SingInPage.scss';

import Banner from '../../components/Banner/Banner';
import SingInForm from '../../components/SingInForm/SingInForm';

import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const SingInPage = () => {
  return (
    <div className="sing-in">
      <Banner title={BANNER_TITLES.SING_IN} gifType={BANNER_TYPES.SING_IN} />
      <SingInForm />
    </div>
  );
};

export default SingInPage;
