import './VerifyListPage.scss';

// components
import Banner from '../../components/Banner/Banner';
import SongList from '../../components/SongList/SongList';

// static
import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const VerifyListPage = () => {
  return (
    <div>
      <Banner
        title={BANNER_TITLES.VERIFY_LIST}
        gifType={BANNER_TYPES.VERIFY_LIST}
      />
      <SongList verify={true} />
    </div>
  );
};

export default VerifyListPage;
