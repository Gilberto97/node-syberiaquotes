import './VerifySongPage.scss';

import Banner from '../../components/Banner/Banner';
import Song from '../../components/Song/Song';

import { BANNER_TYPES, BANNER_TITLES } from '../../static/bannerStatic';

const VerifySongPage = ({ ...rest }) => {
  return (
    <div>
      <Banner title={BANNER_TITLES.VERIFY_SONG} gifType={BANNER_TYPES.VERIFY_SONG} />
      <Song verify={true} {...rest}/>
    </div>
  );
};

export default VerifySongPage;
