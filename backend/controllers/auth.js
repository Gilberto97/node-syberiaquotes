const { validationResult } = require('express-validator')
const bcrypt = require('bcryptjs')
const randomString = require('randomstring')
const jwt = require('jsonwebtoken')
const { sendVerifyEmail } = require('../utils/nodemailer')


const User = require('../models/user')

exports.signup = (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(400).json({ errors: errors.array() });
	}

	const { email, name, password } = req.body
	let userId

	bcrypt.hash(password, 12)
	.then(hashedPw => {
		const user = new User({
			email,
			password: hashedPw,
			name,
			activationToken: randomString.generate(),
		})
		return user.save()
	})
	.then(result => {
		userId = result.id
		return sendVerifyEmail(result.email, result.name, result.activationToken)		
	})
	.then((d) => {
		res.status(201).json({
			message: 'User created!',
			userId
		})
	})
	.catch(err => {
		if (!err.status) {
			err.statusCode = 500
		}
		next(err)
	})
}

exports.signin = (req, res, next) => {
	const errors = validationResult(req);
	if (!errors.isEmpty()) {
		return res.status(400).json({ errors: errors.array() });
	}

	const { email, password } = req.body
	let loadedUser
	User.findOne({ email })
	.then(user => {
		if (!user) {
			const error = new Error('An user with this email could not be found')
			error.statusCode = 401
			throw error
		}
		loadedUser = user
		return bcrypt.compare(password, user.password)
	})
	.then(isEqual => {
		if (!isEqual) {
			const error = Error('Wrong password!')
			error.statusCode = 401
			throw error
		}
		const token = jwt.sign({
			email: loadedUser.email,
			userId: loadedUser.id.toString(),
			isAdmin: loadedUser.isAdmin,
			isStaff: loadedUser.isStaff,
			isActivated: loadedUser.isActivated

		}, process.env.TOKEN_SECRET,
			{ expiresIn: '1h' }
		)
		const { isAdmin, isStaff, isActivated } = loadedUser
		res.status(200).json({ 
			token, 
			userId: loadedUser.id.toString(),
			isAdmin: loadedUser.isAdmin,
			isStaff: loadedUser.isStaff,
			isActivated: loadedUser.isActivated
		})
	})
	.catch(err => {
		if (!err.statusCode) {
			err.statusCode = 500
		}
		next(err)
	})
}

exports.sendVerifyEmail = (req, res, next) => {
	User.findOne({ where: { id: req.userId } })
	.then(user => {
		if (!user) {
			const error = new Error('An user with this id could not be found')
			error.statusCode = 401
			throw error
		}
		
		return user.update({activationToken: randomString.generate()})
	})
	.then(user => {
		return sendVerifyEmail(user.email, user.name, user.activationToken)
	})
	.then(result => {
		res.status(200).json({
			message: 'Activation email sent',
			result: result,
		})
	})
	.catch(err => {
			res.status(500).json({ error: err })
	})
}
