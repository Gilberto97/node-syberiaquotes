const Song = require('../models/song')

exports.getInactiveSongs = (req, res, next) => {
  Song.find({ isActive: false })
  .then(songs => {
    res.status(200).send(songs)
  })
  .catch(error => {
    res.status(500).send({ error })
  })
}

exports.getInactiveSong = (req, res, next) => {
  const { songId } = req.params
  Song.findOne({ $and: [ { _id: songId }, { isActive: false } ] })
    .then(song => {
      if (!song) {
        const error = new Error('Could not find song.')
        error.statusCode = 404
        throw error
      }
      res.status(200).send(song)
    })
    .catch(error => {
      res.status(500).send({ error })
    })
}

exports.activateSong = (req, res, next) => {
  const { songId } = req.params

  Song.findById(songId)
  .then(song => {
    if (!song) {
      const error = new Error('Could not find song.')
      error.statusCode = 404
      throw error
    }
    return Song.updateOne(
      {_id: songId}, 
      { $set: { isActive: true, verifiedBy: req.userId } })
  })
  .then(() => {
    res.status(200).json({
      message: 'Song verified!',
    })
  })
  .catch(error => {
    res.status(500).send({ error })
  })
}