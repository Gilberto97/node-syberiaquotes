const Report = require('../models/report')
const { validationResult } = require('express-validator')

exports.getReports = (req, res, next) => {
  Report.find()
  .then(reports => {
    res.status(200).send(reports)
  })
  .catch(error => {
    res.status(500).send({ error })
  })
}

exports.getReport = (req, res, next) => {
  const { reportId } = req.params
  Report.findById(reportId)
    .then(report => {
      if (!report) {
        const error = new Error('Could not find report.')
        error.statusCode = 404
        throw error
      }
      res.status(200).send(report)
    })
    .catch(error => {
      res.status(500).send({ error })
    })
} 

exports.postReport = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  
  const { title, description, songId } = req.body
  const report = new Report({
    title,
    description,
    songId,
    addedBy: req.userId
  })
  report.save()
  .then(result => {
    res.status(201).json({
      message: 'Report added successfully',
      report: result
    })
  })
  .catch(error => {
    res.status(500).send({ error })
  })
}

// exports.updateSong = (req, res, next) => {
//   const { songId } = req.params
//   const fieldsPossibleToUpdate = ['title', 'lyrics', 'link']
//   let fieldsToUpdate = {}
//   Object.entries(req.body).forEach(([key, value]) => {
//     if (fieldsPossibleToUpdate.includes(key)) {
//       fieldsToUpdate[key] = value
//     }
//   })

//   Song.findById(songId)
//   .then(song => {
//     if (!song) {
//       const error = new Error('Could not find song.')
//       error.statusCode = 404
//       throw error
//     }
//     if (song.addedBy.toString() !== req.userId) {
//       const error = new Error('Not authorized!')
//       error.statusCode = 403
//       throw error
//     }
//     // return song.update(fieldsToUpdate)
//     return Song.updateOne({_id:songId}, { $set: fieldsToUpdate })
//   })
//   .then(() => {
//     res.status(200).json({
//       message: 'Song updated!',
//     })
//   })
//   .catch(error => {
//     res.status(500).send({ error })
//   })
// } 

// exports.activateSong = (req, res, next) => {
//   const { songId } = req.params

//   Song.findById(songId)
//   .then(song => {
//     if (!song) {
//       const error = new Error('Could not find song.')
//       error.statusCode = 404
//       throw error
//     }
//     // return song.update(fieldsToUpdate)
//     return Song.updateOne({_id:songId}, { $set: { isActive: true, verifiedBy: req.userId } })
//   })
//   .then(() => {
//     res.status(200).json({
//       message: 'Song verified!',
//     })
//   })
//   .catch(error => {
//     res.status(500).send({ error })
//   })
// } 

// exports.deleteSong = (req, res, next) => {
//   const songId = req.params.songId
//   Song.findById(songId)
//     .then(song => {
//       if (!song) {
//         const error = new Error('Could not find song.')
//         error.statusCode = 404
//         throw error
//       }
//       if (song.addedBy.toString() !== req.userId) {
//         const error = new Error('Not authorized!')
//         error.statusCode = 403
//         throw error
//       }
//       return Song.deleteOne({ 
//         _id: songId 
//       })
//     })
//     .then(() => {
//       res.status(200).json({ message: 'Song deleted.' })
//     })
//     .catch(error => {
//       if (!error.statusCode) {
//         error.statusCode = 500
//       }
//       next(error)
//     })
// }