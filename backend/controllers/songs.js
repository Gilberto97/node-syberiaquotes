const Song = require('../models/song')
const { validationResult } = require('express-validator')

exports.getActiveSongs = (req, res, next) => {
  Song.find({ isActive: true })
  .then(songs => {
    res.status(200).send(songs)
  })
  .catch(error => {
    res.status(500).send({ error })
  })
}

exports.getActiveSong = (req, res, next) => {
  const { songId } = req.params
  Song.findOne({ $and: [ { _id: songId }, { isActive: true } ] })
    .then(song => {
      if (!song) {
        const error = new Error('Could not find song.')
        error.statusCode = 404
        throw error
      }
      res.status(200).send(song)
    })
    .catch(error => {
      res.status(500).send({ error })
    })
} 

exports.postSong = (req, res, next) => {
  const errors = validationResult(req);
  if (!errors.isEmpty()) {
    return res.status(400).json({ errors: errors.array() });
  }
  
  const { title, lyrics, link } = req.body
  const song = new Song({
    title,
    lyrics,
    link,
    addedBy: req.userId

  })
  song.save()
  .then(result => {
    res.status(201).json({
      message: 'Lyrics added successfully',
      song: result
    })
  })
  .catch(error => {
    res.status(500).send({ error })
  })
}

exports.updateSong = (req, res, next) => {
  const { songId } = req.params
  const fieldsPossibleToUpdate = ['title', 'lyrics', 'link']
  let fieldsToUpdate = {}
  Object.entries(req.body).forEach(([key, value]) => {
    if (fieldsPossibleToUpdate.includes(key)) {
      fieldsToUpdate[key] = value
    }
  })

  Song.findById(songId)
  .then(song => {
    if (!song) {
      const error = new Error('Could not find song.')
      error.statusCode = 404
      throw error
    }
    if (song.addedBy.toString() !== req.userId) {
      const error = new Error('Not authorized!')
      error.statusCode = 403
      throw error
    }
    // return song.update(fieldsToUpdate)
    return Song.updateOne({_id:songId}, { $set: fieldsToUpdate })
  })
  .then(() => {
    res.status(200).json({
      message: 'Song updated!',
    })
  })
  .catch(error => {
    res.status(500).send({ error })
  })
} 

exports.deleteSong = (req, res, next) => {
  const songId = req.params.songId
  Song.findById(songId)
    .then(song => {
      if (!song) {
        const error = new Error('Could not find song.')
        error.statusCode = 404
        throw error
      }
      if (song.addedBy.toString() !== req.userId) {
        const error = new Error('Not authorized!')
        error.statusCode = 403
        throw error
      }
      return Song.deleteOne({ 
        _id: songId 
      })
    })
    .then(() => {
      res.status(200).json({ message: 'Song deleted.' })
    })
    .catch(error => {
      if (!error.statusCode) {
        error.statusCode = 500
      }
      next(error)
    })
}