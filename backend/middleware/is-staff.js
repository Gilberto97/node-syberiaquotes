module.exports = (req, res, next) => {
  console.log("is staff", req.isStaff)
  if (!req.isStaff) {
    const error = new Error('Not authenticated')
    error.statusCode = 401
    throw error
  }
  next()
}