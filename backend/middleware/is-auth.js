const jwt = require('jsonwebtoken')

module.exports = (req, res, next) => {
  const authHeader = req.get('Authorization')
  if (!authHeader) {
    const error = new Error('Not authenticated')
    error.statusCode = 401
    throw error
  }
  const token = authHeader.split(' ')[1]
  const secret = process.env.TOKEN_SECRET
  let decodedToken
  try {
    decodedToken = jwt.verify(token, secret)
  } catch (err) {
    err.statusCode = 500
    throw err
  }

  if (!decodedToken) {
    const error = new Error('Not authenticated')
    error.statusCode = 401
    throw error
  }
  // isAdmin: loadedUser.isAdmin,
	// 		isStaff: loadedUser.isStaff,
	// 		isActivated: loadedUser.isActivated
  // req.userId = decodedToken.userId
  const { userId, isAdmin, isStaff, isActivated } = decodedToken
  req.userId = userId
  req.isAdmin = isAdmin
  req.isStaff = isStaff
  req.isActivated = isActivated
  next()
}