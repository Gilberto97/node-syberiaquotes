const mongoose = require('mongoose');
const Schema = mongoose.Schema

const songSchema = new Schema({
    title: {
        type: String, 
        required: true
    },
    lyrics: {
        type: String, 
        required: true
    },
    link: {
        type: String, 
        required: true
    },
    isActive: {
        type: Boolean,
        default: false
    },
    addedBy: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
        required: true
    },
    verifiedBy: {
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'User', 
    }
}, { timestamps: true })

module.exports = mongoose.model('Song', songSchema)
