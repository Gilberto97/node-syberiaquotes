const mongoose = require('mongoose');
const Schema = mongoose.Schema
const randomString = require('randomstring')

const userSchema = new Schema({
	email: {
		type: String,
		required: true,
		unique: true,
	},
	password: {
		type: String,
		required: true,
	},
	name: {
		type: String,
		required: true,
		unique: true,
	},
	isAdmin: {
		type: Boolean,
		default: false,
	},
	isStaff: {
		type: Boolean,
		default: false,
	},
	activationToken: {
		type: String,
		default: randomString.generate(),
	},
	isActivated: {
		type: Boolean,
		default: false,
	},
	points: {
		type: Number,
		default: 0,
	}
}, { timestamps: true })

module.exports = mongoose.model('User', userSchema)
