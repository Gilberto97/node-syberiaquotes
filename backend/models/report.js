const mongoose = require('mongoose');
const Schema = mongoose.Schema

const reportSchema = new Schema({
  title: {
    type: String, 
    required: true
  },
  description: {
    type: String, 
    required: true
  },
  songId: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'Song', 
    required: true
  },
  addedBy: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User', 
    required: true
  },
  verifiedBy: {
    type: mongoose.Schema.Types.ObjectId, 
    ref: 'User', 
  }
}, { timestamps: true })

module.exports = mongoose.model('Report', reportSchema)
