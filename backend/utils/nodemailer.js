const nodemailer = require('nodemailer')

const smtpTransport = nodemailer.createTransport({
  service: "Gmail",
  host: "smtp.gmail.com",
  port: 587,
  secure: true,
  auth: {
    user: process.env.GMAIL_EMAIL,
    pass: process.env.GMAIL_PASSWORD
  }
});

exports.sendVerifyEmail = (email, name, secretToken) => {
  const mailOptions = {
    from: '"SyberiaQuotes" <syberiaquotes.team@gmail.com>',
    to: email,
    subject: 'Aktywacja Konta',
    html: `
      <body>
        <img src="https://i.ytimg.com/vi/tgOvq4JYJAo/hqdefault.jpg">
        <p>Witaj <b>${name}</b></p> 
        <p>Twój klucz aktywacyjny to: <b>${secretToken}</b>.</p> 
        <p>Aktywuj poprzez link: http://localhost:5000/user/verify/${secretToken}.</p>
      </body>
    `
  }
  return smtpTransport.sendMail(mailOptions)
}

// exports.sendResetPassword = (email, name, password) => {
//   const mailOptions = {
//     from: '"SyberiaQuotes" <syberiaquotes.team@gmail.com',
//     to: email,
//     subject: 'Reset Hasła',
//     text: `Witaj ${name}, Twoje nowe hasło to: ${password}`
//   }
//   return smtpTransport.sendMail(mailOptions)
// }
