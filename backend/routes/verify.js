const express = require('express')

const verifyController = require('../controllers/verify')
const isAuth = require('../middleware/is-auth')
const isStaff = require('../middleware/is-staff')

const router = express.Router()

router.get('', 
	isAuth,
	isStaff,
	verifyController.getInactiveSongs
)
router.post('/:songId', 
	isAuth,
	isStaff,
	verifyController.activateSong
)
router.get('/:songId', 
	isAuth,
	isStaff,
	verifyController.getInactiveSong
)

module.exports = router