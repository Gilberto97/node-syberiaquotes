const express = require('express')
const { body } = require('express-validator')

const authController = require('../controllers/auth')
const isAuth = require('../middleware/is-auth')
const User = require('../models/user')

const router = express.Router()

router.post('/signup',
	body('email')
		.isEmail()
		.withMessage("Please enter a valid email")
		.custom((value) => {
			return User.find({ email: value })
				.then(user => {
					if(user.length) {
						return Promise.reject('E-mail already exists!')
					}
				})
		})
		.normalizeEmail(),
	body('password')
		.trim()
		.isLength({ min: 5 }),
	body('name')
		.trim()
		.not()
		.isEmpty()
		.custom((value) => {
			return User.find({ name: value })
				.then(user => {
					if(user.length) {
						return Promise.reject('Name already exists!')
					}
				})
		}),
	authController.signup
)

router.post('/signin', 
	body('email')
		.isEmail()
		.withMessage("Please enter a valid email")
		.normalizeEmail(),
	authController.signin
)

router.post('/sendverifyemail', isAuth, authController.sendVerifyEmail)

module.exports = router
