const express = require('express')
const { body } = require('express-validator')

const reportsController = require('../controllers/reports')
const isAuth = require('../middleware/is-auth')
const isStaff = require('../middleware/is-staff')
const Song = require('../models/song')

const router = express.Router()

router.get('', 
	isAuth,
	isStaff,
	reportsController.getReports
)
router.get('/:reportId', 
	isAuth,
	isStaff,
	reportsController.getReport
)

router.post('', 
	isAuth,
	isStaff,
	body('title')
		.trim()
		.not()
		.isEmpty()
		.isLength({ min: 5 }),
	body('description')
		.trim()
		.not()
		.isEmpty()
		.isLength({ min: 5 }),
	body('songId')
		.trim()
		.not()
		.isEmpty()
		.custom((value) => {
			console.log("VALUE", value)
			return Song.findById(value)
				.then(song => {
					if(!song) {
						return Promise.reject('Song you want to report does not exists!')
					}
				})
				.catch(err => {
					return Promise.reject('Song you want to report does not exists!')
				})
		}),
	reportsController.postReport
)
// router.post('/verify/:songId', 
// 	isAuth,
// 	isStaff,
// 	songsController.activateSong
// )
// router.patch('/:songId', isAuth, songsController.updateSong)
// router.delete('/:songId', isAuth, songsController.deleteSong)

module.exports = router