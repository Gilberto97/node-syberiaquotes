const express = require('express')
const { body } = require('express-validator')

const songsController = require('../controllers/songs')
const isAuth = require('../middleware/is-auth')
const isStaff = require('../middleware/is-staff')
const Song = require('../models/song')

const router = express.Router()

router.get('', songsController.getActiveSongs)
router.post('', 
	isAuth,
	body('title')
		.trim()
		.not()
		.isEmpty()
		.isLength({ min: 5 })
		.custom((value) => {
			return Song.find({ title: value })
				.then(user => {
					if(user.length) {
						return Promise.reject('Song with that title already exists!')
					}
				})
		}),
	body('lyrics')
		.trim()
		.not()
		.isEmpty()
		.isLength({ min: 5 }),
	body('link')
		.trim()
		.not()
		.isEmpty()
		.isLength({ min: 5 })
		.custom((value) => {
			return Song.find({ link: value })
				.then(user => {
					if(user.length) {
						return Promise.reject('Song with that link already exists!')
					}
				})
		}),
	songsController.postSong
)
router.get('/:songId', songsController.getActiveSong)
router.patch('/:songId', isAuth, songsController.updateSong)
router.delete('/:songId', isAuth, songsController.deleteSong)

module.exports = router