const mongoose = require('mongoose') 
const server = require('./app')

mongoose.connect(
	`mongodb://${process.env.MONGO_INITDB_ROOT_USERNAME}:${process.env.MONGO_INITDB_ROOT_PASSWORD}@mongodb:27017/syberiaquotes?authSource=admin`,
	{
		useNewUrlParser: true,
		useUnifiedTopology: true,
	},
	(err) => {
		if (err) {
			console.error('FAILED TO CONNECT TO MONGODB');
			console.error(err);
		} else {
			server.listen(3000);
		}
	}
);