const http = require('http')
const express = require('express')
const bodyParser = require("body-parser")
const helmet = require('helmet')
const morgan = require('morgan')

const songsRoutes = require('./routes/songs')
const verifyRoutes = require('./routes/verify')
const reportsRoutes = require('./routes/reports')
const authRoutes = require('./routes/auth')

const app = express()
const server = http.createServer(app)

app.use(helmet())
app.use(morgan('combined'))
app.use(bodyParser.json())

app.use((req, res, next) => {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Methods', 'GET, POST, PUT, PATCH, DELETE')
	res.setHeader('Access-Control-Allow-Headers', 'Content-Type, Authorization')
	next()
})

app.use('/songs', songsRoutes)
app.use('/verify', verifyRoutes)
app.use('/reports', reportsRoutes)
app.use('/auth', authRoutes)

app.use((error, req, res, next) => {
	const status = error.statusCode || 500
	console.log(error)
	res.status(status).json({ error })
})

module.exports = app
